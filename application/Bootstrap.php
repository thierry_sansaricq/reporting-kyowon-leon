<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
    
    protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions(), true);
        Zend_Registry::set('config', $config);
        return $config;
    }    
    
    
    /**
     * Initialize logging mechanism.
     *
     * This method checks to see if we are in a production environment or not. If
     * a production environment is in use, it writes log messages to a log file
     * located at /data/logs/app.log. If it is any other environment, it logs
     * messages to FirePHP. For use in controllers, models, and views; use:
     *      $logger = Zend_Registry::get('logger');
     *      $logger->debug('A debug message here');
     *      $logger->info('A log message here');
     *
     * To log the instantiation of methods, the following snippet can be used:
     *      $this->_logger->info('Bootstrap ' . __METHOD__);
     *
     *
     * @access protected
     */
    protected function _initLogging() {
        $this->bootstrap('frontController');
        
        $config = Zend_Registry::get('config');

        $logger = new Zend_Log();
        $logger->setTimestampFormat("d.m.Y H:i:s");
        
        // Logging enabled
        if($config->options->debug) {
            
            $logfile = str_replace('{APPLICATION_PATH}', APPLICATION_PATH, $this->config->options->debuglogfile);

            $writer = new Zend_Log_Writer_Stream($logfile);
            $logger->addWriter($writer);
        }
        
        // Firebug concole enabled
        if($config->options->firebug) {
            $firebug = new Zend_Log_Writer_Firebug();
            $logger->addWriter($firebug);            
        }

        Zend_Registry::set('logger', $logger);
    }
    
    /**
     * Initialize database profiling using Firebug.
     *
     * All database queries will be dumped to FirePHP if it is not a production environment.
     *
     * @access protected
     */
    protected function _initDbProfiler() {
        
        $config = Zend_Registry::get('config');
        
        if($config->options->dbprofiler) { 
            $this->bootstrap('db');
            $profiler = new Zend_Db_Profiler_Firebug('All DB Queries');
            $profiler->setEnabled(true);
            $this->getPluginResource('db')
                ->getDbAdapter()
                ->setProfiler($profiler);            
        }
    }    
    
    
    /**
     * Initialize auto loading of Speakaboos Library
     *
     * @access protected
     */
    protected function _initSpeakaboosLibraryAutoload() {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('Speakaboos_');

        return $autoloader;
    }
    

    
    
}