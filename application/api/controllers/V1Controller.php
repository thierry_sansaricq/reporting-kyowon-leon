<?php                                                                           
/**                                                                             
 * API Index Controller                                                             
 *                                                                              
 * Max Chernopolsky   max@speakboos.com
 * @copyright  Copyright (c) 2013 Speakaboos, LLC. (http://www.speakaboos.com)  
 * @version   
 */
class V1Controller extends Speakaboos_Api_Controller {

    

  
    
    /**
    * Returns data describing a student's story reading performance.
    * The student username (un) is required.  This is the KyoWon user ID passed to our app during the authentication phase.  The start and end date parameters are optional.  If they are not included, we will default to the last 30 days.
    * 
    */
    public function studentAction() {
        
        
        $userName  = $this->getRequest()->getParam('un'); // @sample someUserName kyowon user name       
   
        $startTime = (int) $this->getRequest()->getParam('st'); // @sample 1382717138 Start date (Unix timestamp). If empty, current month will be displayed 
        $endTime   = (int) $this->getRequest()->getParam('et'); // @sample 1383149138 End date (Unix timestamp)        
        $partnerId = (int) $this->getRequest()->getParam('p'); // @sample 1 Partner id. 1 for Kyowon            
        
        
        // Setting default time range if parameters st and et not specified
        if(!$startTime && !$endTime) {
            $days = (int) $this->config->options->dafault_time_range;
            $startTime = strtotime("-$days day");
        }


        $viewModel = new Speakaboos_Stats_Model_Stat();
        
        if($userName) {
            $data = $viewModel->getStudentViews($userName, false, $startTime, $endTime);
        } elseif($userId) {
            $data = $viewModel->getStudentViews(false, $userId, $startTime, $endTime);
        } else {
            $this->error('No user id or username passed');
        }
        
        
        
        $this->ok($data);
        
    }
    
    
    /**
    * Returns aggregate data across all users for last 24 hours
    * 
    */
    public function dailyAction() {
        
        $viewModel = new Speakaboos_Stats_Model_Stat();
                       
        $data = $viewModel->getStudentViews(false, false, false, false, true);
        
        $this->ok($data);
    }
    
    /**
    * Returns aggregate data across all users (not specific to any one user).  This service is currently configured to return data based on a given month and year
    * This service currently returns two useful pieces of data: most accessed genre and most accessed mode.
    */    
    public function monthlyAction() {
        
        $year      = (int) $this->getRequest()->getParam('year'); // @sample 2013 Report year
        $month     = (int) $this->getRequest()->getParam('month'); // @sample 12 Report month (integer: 1-12)
        $partnerId = (int) $this->getRequest()->getParam('p'); // @sample 1 Partner id. 1 for Kyowon       

        if(!$year) {
            $year = date('Y');
        }
        
        if(!$month) {
            $month = date('m');
        }
        
        $category = false;
        $mode = false;
        
        $statModel = new Speakaboos_Stats_Model_Stat();
        $result = $statModel->getMostAccessedCategory($year, $month);
        
        if($result) {
            $category = $result['name'];
        }
        
        $result = $statModel->getMostAccessedMode($year, $month);
        
        if($result) {
            $mode = $result['name'];
        }        
        
        
        $data = array(
            'year'  => $year,
            'month' => $month,
            'most_accessed_genre' => $category,
            'most_accessed_mode' => $mode,        
        );
        
        
        $this->reply($data);
        
    }
    
    
    // Measure execution time
    /*
    public function __destruct() {
        
        if($this->config->options->debug) {
            global $appStartTime;
            @list( $startMilli, $startSeconds, $endMilli, $endSeconds) = explode(' ',$appStartTime . ' ' . microtime());
            $generateTime = ($endSeconds+$endMilli)-($startSeconds+$startMilli);
        
            $timeDebug = sprintf('Executed in %.3fs', $generateTime);
            
            echo "DEBUG: $timeDebug";            
        }
    } 
    */    


}
