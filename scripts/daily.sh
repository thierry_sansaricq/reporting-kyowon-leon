#!/bin/bash
# This script requests Kyowon daily report from reporting web srvice
# And saves it into compressed file. Then put it into Amazon S3 bucket
# s3cmd should be configured http://tecadmin.net/install-s3cmd-manage-amazon-s3-buckets/


if [ $# -eq 0 ]
  then
    echo "Environment argument missed. Usage: ./daily.sh development|staging|production"
	exit
fi

# Environment
if [ $1 == "development" ]; then

	# Reporting API URL
	API="http://reporting/?method=daily"
	# Path where to store logs locally
	LOGPATH="../public/logs/" 
	# Amazon S3 destination folder where to store logs
	S3PATH="s3://kyowon/logs/dev/"	
	# Report URL base
	URL="https://s3.amazonaws.com/kyowon/logs/dev/"
	
elif [ $1 == "staging" ]; then
	
	# Reporting API URL
	API="http://staging.reporting.speakaboos.com/?method=daily"
	# Path where to store logs locally
	LOGPATH="/tmp/" 	
	# Amazon S3 destination folder where to store logs
	S3PATH="s3://kyowon/logs/dev/"
	# Report URL base
	URL="https://s3.amazonaws.com/kyowon/logs/dev/"
	
elif [ $1 == "production" ]; then

	# Reporting API URL
	API="http://reporting.speakaboos.com/?method=daily"
	# Path where to store logs locally
	LOGPATH="/tmp/" 
	# Amazon S3 destination folder where to store logs
	S3PATH="s3://kyowon/logs/daily/"	
	# Report URL base
	URL="https://s3.amazonaws.com/kyowon/logs/daily/"	
	
else 
	echo "Unknown environment: ${APPLICATION_ENV}"
fi



# Report filename with current date
NOW=$(date +"%Y-%m-%d")
FILE="kyowon.daily.$NOW"

# Full ��� to logfile
LOGFILE=${LOGPATH}${FILE}

echo "Requesting: $API"
echo "Saving: ${LOGFILE}"
echo ""

# Get daily report from reporting service
curl $API > ${LOGFILE}.xml
gzip ${LOGFILE}.xml
mv ${LOGFILE}.xml.gz ${LOGFILE}.gz




echo ""
echo "Sending file to S3"

# Send report to s3 bucket
s3cmd -P put ${LOGFILE}.gz ${S3PATH}


echo "Deleting tmp file"
rm ${LOGFILE}.gz

echo "Done!"
